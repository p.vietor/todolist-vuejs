export enum TodoStatus {
    TODO,
    DOING,
    DONE,
  }

export class Todo {
    id: string
    content: string
    status: TodoStatus

    constructor(id: string, content: string, status: TodoStatus) {
        this.id = id;
        this.content = content;
        this.status = status;
    }
}

export class TodoList {
  todos: Todo[];

  constructor(todos: Todo[]) {
    this.todos = todos;
  }

  add(todo: Todo) {
    this.todos.push(todo);
  }

  remove(id: string) {
    this.todos = this.todos.filter(t => t.id !== id);
  }

  getById(id: string) {
    return this.todos.filter(t => t.id === id)[0];
  }

  isDone(id: string) {
    return this.getById(id).status === TodoStatus.DONE;
  }

  setStatus(id: string, status: TodoStatus) {
    this.getById(id).status = status;
  }

  resetList(todos: Todo[]) {
    this.todos = todos;
  }

}