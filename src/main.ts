import { createApp } from 'vue'
import App from './App.vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrashCan, faPlus } from '@fortawesome/free-solid-svg-icons'
import './assets/main.css'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


library.add(faTrashCan);
library.add(faPlus);


createApp(App)
  .component('font-awesome-icon', FontAwesomeIcon)
  .mount('#app');
